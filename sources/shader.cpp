#include "shader.h"


Shader::Shader(std::string vertexShaderFilename, std::string fragShaderFilename)
{
    unsigned int vertexShader, fragShader;
    std::string responseShader;

    vertexShader = CreateShader(vertexShaderFilename, GL_VERTEX_SHADER, responseShader);
    printf("\n vertext shader result is: %s\n", responseShader.c_str());
    fragShader = CreateShader(fragShaderFilename, GL_FRAGMENT_SHADER, responseShader);
    printf("frag shader result is: %s\n", responseShader.c_str());


    shaderId = glCreateProgram();
    glAttachShader(shaderId, vertexShader);
    glAttachShader(shaderId, fragShader);
    glLinkProgram(shaderId);

    glDeleteShader(vertexShader);
    glDeleteShader(fragShader);
}

Shader::~Shader()
{
//    glDeleteProgram(shaderId);
}


//read shader from file and return it's compiled id if successful or an error's text in a referenced variable
uint Shader::CreateShader(std::string shaderFilename, GLuint shaderType, std::string & responseOut)
{
    std::string shaderline;
    std::string shaderString;
    std::ifstream shaderFile(shaderFilename);

    std::ostringstream sstr;
    sstr << shaderFile.rdbuf();
    shaderString = sstr.str() + "\n\0";
    const char * shaderChars = shaderString.c_str();

    shaderFile.close();
/*         if(printShader)
    {
        printf("resulted shader is: \n");
        printf(shaderString.c_str());
    }
*/        
    auto checkShaderCompileSuccess = [](uint shaderId, std::string shaderName, std::string & response)
    {
        int  success;
        char infoLog[512];
        glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
        response = "OK";
        if(!success)
            {
                glGetShaderInfoLog(shaderId, 512, NULL, infoLog);
                printf("shader '%s' compilation failed!\n", shaderName.c_str());
                printf("error: %s", infoLog);
                printf("\n");
                response = infoLog;
            }
        return success;
    };

    unsigned int shaderId;
    shaderId = glCreateShader(shaderType);

    //IMPORTANT!!! shaderChars is char * so it is in a sense not an array of chars but a pointer to some other place 
    //  so the object that it references to must exist at the moment we are trying to use it somewhere
    glShaderSource(shaderId, 1, &shaderChars, NULL);
    glCompileShader(shaderId);
    if(!checkShaderCompileSuccess(shaderId, shaderFilename, responseOut))
    {
        printf("shader text: \n");
        std::string temp1 = shaderChars;
        printf(temp1.c_str());
        std::cout << shaderChars << std::endl;
        printf("\n");
    }

    return shaderId;

};

void Shader::Use()
{
    glUseProgram(shaderId);
}

void Shader::SetBool(const std::string &name, bool value) const
{
    glUniform1i(glGetUniformLocation(shaderId, name.c_str()), (int)value); 
} 
void Shader::SetInt(const std::string &name, int value) const
{
    glUniform1i(glGetUniformLocation(shaderId, name.c_str()), value); 
}   
void Shader::SetFloat(const std::string &name, float value) const
{
    glUniform1f(glGetUniformLocation(shaderId, name.c_str()), value); 
}
void Shader::SetFloat4(const std::string &name, float x, float y, float z, float w) const
{
    glUniform4f(glGetUniformLocation(shaderId, name.c_str()), x, y, z, w); 
}