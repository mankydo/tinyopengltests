#include <iostream>
#include <fstream>
#include <sstream> 
#include "glTriangle.h"

glTriangle::glTriangle(float vertices[], uint verticesSize, int numberOfAttributes)
{

    glGenBuffers(1, &VBO);
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);


    // creating a buffer with vertex data
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_STATIC_DRAW);

    if(numberOfAttributes > 0)
    {
        int i = 0;
        int size = 3;
        int stride = numberOfAttributes * size;

         while(i < numberOfAttributes) 
         {
            int offset = size * i;
            int index = i;
            // assigning vertex atrributes, like location and it's order to read from memory(in this case it's float per coord with no offsets or skips)
            glVertexAttribPointer(index, size, GL_FLOAT, GL_FALSE, stride * sizeof(float), (void *)(offset * sizeof(float)));
            glEnableVertexAttribArray(index);
            printf(" number of argument is: %i, offset: %i, stride: %i\n", i, offset, stride);
            i++;
        }
        printf(" final number of argument is: %i\n", i);
    }

    // disconecting vertex buffer and vertex array object from our state machine
    glBindBuffer(GL_ARRAY_BUFFER, 0); 
    glBindVertexArray(0); 
}

glTriangle::~glTriangle()
{
/*     glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
 */}

uint glTriangle::GetNumberOfVertices()
{}

void glTriangle::Draw()
{
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}


void glTriangle::UnbindObject()
{}
