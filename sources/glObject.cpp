#include <iostream>
#include <fstream>
#include <sstream> 
#include "glObject.h"

glObject::glObject(float vertices[], uint verticesSize, uint indices[], uint indicesSize)
{

    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);


    // creating a buffer with vertex data
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_STATIC_DRAW);

    // creating an element buffer with vertices' indices to draw, well, elements i.e. polygonal objects
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, indices, GL_STATIC_DRAW);

    // assigning vertex atrributes, like location and it's order to read from memory(in this case it's float per coord with no offsets or skips)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);

    // disconecting vertex buffer and vertex array object from our state machine
    glBindBuffer(GL_ARRAY_BUFFER, 0); 
    glBindVertexArray(0); 
}

glObject::~glObject()
{
/*     glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
 */}

uint glObject::GetNumberOfVertices()
{}

void glObject::Draw()
{
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

}


void glObject::UnbindObject()
{}