#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 
#include <math.h>
#include "stb_image.h"
#include "glObject.h"
#include "glTriangle.h"
#include "shader.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

std::string shaderDirectory = "shaders";

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); 
  
    GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }


    Shader simpleShader = Shader(shaderDirectory + "/vertex.glsl", shaderDirectory + "/frag.glsl");


    //the first triangle, vbo and so on
    float vertices[] = {
        0.1f,  0.5f, 0.0f,  // top right
        0.1f, -0.5f, 0.0f,  // bottom right
        -0.8f, -0.5f, 0.0f,  // bottom left
        -0.8f,  0.5f, 0.0f   // top left 
    };

    float vertices2[] = {
        0.9f,  0.5f, 0.0f,  // top right
        0.9f, -0.5f, 0.0f,  // bottom right
        0.3f, -0.5f, 0.0f,  // bottom left
        0.3f,  0.5f, 0.0f   // top left 
    };

    unsigned int indices[] = {  // note that we start from 0!
        0, 1, 3,   // first triangle
        1, 2, 3    // second triangle
    };

    glObject simpleRct = glObject(vertices, sizeof(vertices), indices, sizeof(indices));
    glObject simpleRct2 = glObject(vertices2, sizeof(vertices2), indices, sizeof(indices));

    float verticesTriangle[] = {
        0.9f,  0.5f, 0.0f,  // top right
        0.9f, -0.5f, 0.0f,  // bottom right
        0.3f, -0.5f, 0.0f,  // bottom left
    };

    float verticesTriangle2[] = {
    // positions         // colors
        0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // bottom left
        0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // top 
    };

    glTriangle triangle = glTriangle(verticesTriangle, sizeof(verticesTriangle));

    glTriangle triangle2 = glTriangle(verticesTriangle2, sizeof(verticesTriangle2), 2);

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); 

    //render loop
    while(!glfwWindowShouldClose(window))
    {
        processInput(window);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        float time = glfwGetTime();
        float greenVal = (sin(time) / 2  + .5 );

        simpleShader.Use();

        simpleShader.SetFloat4("ourColor", .1, greenVal, .1, 1);

        simpleRct.Draw();
        triangle.Draw();
        triangle2.Draw();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
   

    return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

//Proceed input from the window
void processInput(GLFWwindow * window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        printf("ESC pressed!\n");
        glfwSetWindowShouldClose(window, true);
    }
        if(glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
    {
        printf("Left pressed!\n");
        
        glfwSetWindowShouldClose(window, true);
    }
}
