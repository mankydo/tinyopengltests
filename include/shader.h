
#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream> 
#include "glad/glad.h"

class Shader
{
private:
    /* data */

protected:
    uint CreateShader(std::string shaderFilename, GLuint shaderType, std::string & printShader);

public:

    uint shaderId;

    Shader(std::string vertexShaderFilename, std::string fragShaderFilename);
    ~Shader();
    
    void Use();
    // utility uniform functions
    void SetBool(const std::string &name, bool value) const;  
    void SetInt(const std::string &name, int value) const;   
    void SetFloat(const std::string &name, float value) const;
    void SetFloat4(const std::string &name, float x, float y, float z, float w) const;
};

#endif