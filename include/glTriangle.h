#ifndef GLTRIANGLE_H
#define GLTRIANGLE_H

#include "glad/glad.h"
#include <string>

typedef unsigned int uint;

class glTriangle
{
private:

public:

    uint objectId;
    uint VBO, VAO;

    glTriangle(float vertices[], uint verticesSize, int numberOfAttributes = 1);
    ~glTriangle();

    uint GetNumberOfVertices();
    void Draw();
    void UnbindObject();
};

#endif