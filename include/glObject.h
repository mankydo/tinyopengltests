#ifndef GLOBJECT_H
#define GLOBJECT_H

#include "glad/glad.h"
#include <string>

typedef unsigned int uint;

class glObject
{
private:

public:

    uint objectId;
    uint VBO, VAO, EBO;

    glObject(float vertices[], uint verticesSize, uint indices[], uint indicesSize);
    ~glObject();

    uint GetNumberOfVertices();
    void Draw();
    void UnbindObject();
};

#endif