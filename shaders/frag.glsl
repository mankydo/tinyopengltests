#version 330 core
out vec4 FragColor;
in vec3 vertexColor;

uniform vec4 ourColor;

void main()
{

//    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
    vec4 colorAdd = vec4(vertexColor.xyz, 1);
    colorAdd.w = 1;
    FragColor = ourColor + colorAdd;
}

