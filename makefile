	
CXX=g++
CFLAGS=-g -pthread -ldl -lGL -lrt -lXrandr -lXxf86vm -lXi -lXinerama -lX11 -lXcursor -lglfw3
INCLUDE=-I
SRCDIR=sources/
OBJDIR=obj/
LIBDIR=libs/
INCLUDEDIR=include/
SOURCES=$(wildcard $(SRCDIR)*.cpp)
DEPS = 

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	$(CXX) -c $< -o $@ -I$(INCLUDEDIR) -L$(LIBDIR)
	@echo "\n sources are: " $(SOURCES)
	@echo "\n get arg: "$@" and "$<""
	@echo "   Object created:" $@
	@echo "\n"

basicwindow: $(SOURCES:$(SRCDIR)%.cpp=$(OBJDIR)%.o)
	$(CXX) -o basicwindow $(LIBDIR)/glad.c $^ -I$(INCLUDEDIR) -L$(LIBDIR) $(CFLAGS)
	@echo "\nCompiled basicwindow successfuly!"



